﻿  var logField     = null,chatField = null;
  var IMInputField = null;
  var serverName = 'm.kindcity.ru'; // define chat server name
  var SessionID = null;
  var userName = 'guest'; // define guest chat account credentials
  var password = 'guest';
  var fileSizeLimit = 1048576; // define max file size in bytes
  var fileStorePath = '/private/settings/imFiles/'; // define path to store transmitted files
  var fileSharePath = '/protected/settings/imFiles/pwd/'; // define path to share transmitted files

  $(document).ready(function() {
    // <drag&drop>
    $('#fixed-block-chat').on('dragenter', function (e) {
      e.stopPropagation();
      e.preventDefault();
    });
    $('#fixed-block-chat').on('dragover', function (e) {
      e.stopPropagation();
      e.preventDefault();
    });
    $('#fixed-block-chat').on('drop', function (e) {
      e.preventDefault();
      var files = e.originalEvent.dataTransfer.files;
      if(SessionID) handleFileUpload(files);
    });
    // </drag&drop>
    // <file_upload>
    $('#sendFileButton').on('click', function() {
      $('#fileInput').trigger('click');
    });
    $('#fileInput').on('change', function() {
      var files =  $('#fileInput')[0].files;
      if(SessionID) handleFileUpload(files);
    });
    function handleFileUpload(files) {
      for (var i = 0; i < files.length; i++) {
        if(files[i].size < fileSizeLimit) {
          var fd = new FormData();
          fd.append('fileData', files[i]);
          sendFileToServer(fd, files[i].name);
        }
      }
    }
    function sendFileToServer(formData, fileName) {
      var dt = new Date();
      var uploadID = dt.getTime();
      $.ajax({
        url: 'http://' + serverName + '/Session/' + SessionID + '/UPLOAD/' + uploadID,
        type: 'POST',
        contentType:false,
        processData: false,
        data: formData,
        success: function(data) {
          if(/OK/.test(data)) {
            if(myController != null) myController.storeFile(uploadID, fileName);
          }
        },
      });
    }
    // </file_upload>
  });

  var urlParams = {};
  { var x = location.search.indexOf("?");
    if(x >= 0) {
      var params = location.search.substr(x+1).split("&");
      for(x = 0; x < params.length; ++x) {
        var temp = params[x].split("=");
        urlParams[unescape(temp[0])] = unescape(temp[1]) || "";
      }
    }
  }

  window.debugLog = function(tag,data) {
    if(logField == null) {
      logField = window.document.getElementById("logField");
      if(logField == null || (logField = logField.firstChild) == null) return;
    }
   
   if(tag == null & data == null) {
    logField.nodeValue = "";
   } else {
     var thisTime = new Date;
     logField.nodeValue += 
       ((thisTime.getHours()   + 100) + ":").substring(1) +
       ((thisTime.getMinutes() + 100) + ":").substring(1) +
       ((thisTime.getSeconds() + 100) + ".").substring(1) +
       ((thisTime.getMilliseconds() + 1000)+" ").substring(1) +
       tag + ": " + data + "\r\n";
   }
  } 

  window.UIAddToChatLog = function(nickName,isSelf,text,textClass) {
    if(chatField == null) {
      chatField = window.document.getElementById("chatField");
      if(chatField == null) return;
    }

    var thisTime = new Date;
    var lineNode = window.document.createElement("div");
    if ( isSelf ) {
        lineNode.className = 'row to';
    } else {

        var notifyFiled = window.document.getElementById("notifyField");

        //window.document.getElementById("notifyField").appendChild(nicknameBLock);
        lineNode.className = 'row from';
    }

    var dateWrapper = window.document.createElement("div");
    dateWrapper.className = 'date-wrapper';

    var date = window.document.createElement("span");
    date.className = 'date';

    var pushDate = window.document.createElement("div");
    pushDate.className = 'push';
    dateWrapper.appendChild(pushDate);

    date.appendChild(window.document.createTextNode(
            ((thisTime.getHours()   + 100) + ":").substring(1) + 
            ((thisTime.getMinutes() + 100) + "").substring(1) ));
            //((thisTime.getSeconds() + 100) + " ").substring(1) ));
    dateWrapper.appendChild(date);
    lineNode.appendChild(dateWrapper);
    if(nickName != null) {
      var subNode = window.document.createElement("span");
      subNode.appendChild(window.document.createTextNode(nickName+":"));
      //lineNode.appendChild(subNode);
      subNode.className = isSelf ? "name" : "name"; // subNode.setAttribute("class",isSelf ? "selfNickName" : "nickName");
    }


    if(textClass != null) {
      var textWrapper =  window.document.createElement("div");
      textWrapper.className = textClass;  //subNode.setAttribute("class",textClass);

      var subNode = window.document.createElement("span");
      if(/^https{0,1}\:\/\/[^\s]*$/.test(text)) {
        var aNode = window.document.createElement("a");
        aNode.href = text;
        aNode.target = '_blank';
        aNode.appendChild(window.document.createTextNode(decodeURIComponent(text.split('/').pop())));
        subNode.appendChild(aNode);
      } else {
        subNode.appendChild(window.document.createTextNode(text));
      }
      subNode.className =  'text';
      textWrapper.appendChild(subNode);
      lineNode.appendChild(textWrapper);

    } else {
      lineNode.appendChild(window.document.createTextNode(text));
    }
    chatField.appendChild(lineNode);
    chatField.scrollTop = chatField.scrollHeight;
  }

  var centerAddress      = null;
  var ourNickName        = null;
  var connectImmediately = false;

  function XIMSSController() {
    var that         = this;
    var ximssSession = null;
    var chatConnected= false;

    function debugLog(x) {window.debugLog("USER",x);}

    function loginCallback(newSession,errorCode) {
      debugLog("loginCallback called");
      if(errorCode != null) {
        debugLog("login failed: "+ errorCode);
        UIAddToChatLog(null,false,"*** login failed: " + errorCode + " ***","msg client");
        return;
      }
      ximssSession = newSession;
      ximssSession.setDebugFunction(window,window.debugLog);
      debugLog("session created");
      ximssSession.setUnknownAsyncProcessor(that,ximssAsyncProcessor);
      ximssSession.setNetworkErrorProcessor(that,ximssNetworkErrorProcessor);

      ximssSession.setAsyncProcessor(that,ximssReadIMProcessor,    "readIM");
      ximssSession.setAsyncProcessor(that,ximssReadXMPPIQProcessor,"iqRead");
      ximssSession.setAsyncProcessor(that,ximssPresenceProcessor,  "presence");

      

      ximssSession.start();
      debugLog("session started");
  
      ximssSession.sendRequest(ximssSession.createXMLNode("readTime"),that,readTimeCallback,ximssOpCallback,false);

      xmlBindRequest = ximssSession.createXMLNode("signalBind");
      xmlBindRequest.setAttribute("readIM","1");
      ximssSession.sendRequest(xmlBindRequest,that,null,ximssOpCallback,true);
 
     //UIAddToChatLog(null,false,"*** logged in ***","msg client");
     if(connectImmediately == "yes") connectToCenter();
   }

   function sendOurPresence(presenceState,sendImmediately) {
     var xmlConnectRequest = ximssSession.createXMLNode("presenceSet");
     if(centerAddress == null) {ximssOpCallback("center address not defined",xmlConnectRequest); return;}

     xmlConnectRequest.setAttribute("peer",centerAddress);
     xmlConnectRequest.setAttribute("clientID",ourNickName);
     if(presenceState == "offline") {
       xmlConnectRequest.setAttribute("type","unavailable");
     } else {
       xmlPresence = ximssSession.createXMLNode("presence"); xmlPresence.appendChild(ximssSession.createTextNode(presenceState));
       xmlConnectRequest.appendChild(xmlPresence);
     }
     ximssSession.sendRequest(xmlConnectRequest,that,null,ximssOpCallback,sendImmediately);
   }

   function connectToCenter() {
     if(chatConnected) return;
     debugLog("connectToCenter started");     
     sendOurPresence("online",true);
   }

   function ximssPresenceProcessor(xmlData) {
     var clientID     = xmlData.getAttribute("clientID");
     if ( clientID != ourNickName ) {
         UINotifyComposing(clientID);

         xmlRequest = ximssSession.createXMLNode("iqSend");
         xmlRequest.setAttribute("peer",   xmlData.getAttribute("peer"));
         xmlRequest.setAttribute("clientId",clientID);
         xmlRequest.setAttribute("type","get");
         xmlRequest.setAttribute("iqid","4");
//         var vcard = ximssSession.createXMLNode('vCard');
//         vcard.setAttribute('xmlns','vcard-temp');
//         xmlRequest.appendChild(vcard);
         var vcard = new DOMParser().parseFromString("<vCard xmlns='vcard-temp'/>", "text/xml");
         xmlRequest.appendChild(vcard.firstChild);
         ximssSession.sendRequest(xmlRequest,that,null,ximssOpCallback,true);
//
//         xmlRequest = ximssSession.createXMLNode("fileRead");
//         xmlRequest.setAttribute("type", 'vcard');
//         xmlRequest.setAttribute("fileName", 'profile.vcf');
//         ximssSession.sendRequest(xmlRequest,that,null,ximssOpCallback,true);
//
     }
     var textPresence = xmlData.getAttribute("type");
     if(textPresence == "unavailable") {
       textPresence = "offline";
     } else {
       var xmlPresence  = xmlData.getElementsByTagName("presence")[0];
       textPresence = xmlPresence != null ? xmlPresence.firstChild.nodeValue : null;
     }

     if(xmlData.getAttribute("peer") != centerAddress) {

       debugLog("unknown presence source:" + String(xmlData.getAttribute("peer")));
     } else if(clientID == ourNickName) {
       debugLog("self-presence received:" + String(textPresence));
       if(textPresence == "offline") onChatDisconnected();
       else                          onChatConnected();

     } else if(clientID == null && xmlData.getAttribute("type") == "error") {
       UIAddToChatLog(null,false,"*** failed to connect to the center, try a different nickname","serverError");
       
     } else {
       //if(textPresence == "offline") {UIAddToChatLog(clientID,false,"*** disconnected","serverAnnounce");}
       //else                          {UIAddToChatLog(clientID,false,"*** connected (" + textPresence + ")","server msg");}
     }

   }

   function onChatConnected() {
     //UIAddToChatLog(null,false,"*** you have been connected",   "server msg");
     if(!chatConnected) {
       chatConnected = true;
       UIOnChatConnected(); 
       if(delayedIM != null) {sendIM(delayedIM); delayedIM = null;}
     }
   }

   function onChatDisconnected() {
     UIAddToChatLog(null,false,"*** you have been disconnected","serverError");
     UIOnChatDisconnected();
     chatConnected = false;
   }

   function readTimeCallback(xmlResponse,xmlRequest) {
     debugLog("time data read:"+xmlResponse.getAttribute("localTime"));
   }

   function ximssOpCallback(errorCode,xmlRequest) {
     debugLog(xmlRequest.tagName + " completed" + (errorCode != null ? ". errorCode=" + errorCode : ""));
     if(errorCode != null) {
       UIAddToChatLog(null,false,errorCode + " (" + xmlRequest.tagName + ")","serverError");
     }
   }

   var delayedIM = null;

   function ximssConnectCallback(errorCode,xmlRequest) {
     ximssOpCallback(errorCode,xmlRequest);
   }


   function ximssSendIMCallback(errorCode,xmlRequest) {
     ximssOpCallback(errorCode,xmlRequest);
     sendIMCompleted(errorCode);
   }

   var fileName = null;

   this.storeFile = function(uploadID, fN) {
     if(ximssSession == null) return;
     fileName = fN;
     
     xmlRequest = ximssSession.createXMLNode("fileStore");
     xmlRequest.setAttribute("fileName",fileStorePath + fileName);
     xmlRequest.setAttribute("uploadID",uploadID);

     ximssSession.sendRequest(xmlRequest,that,null,ximssStoreFileCallback,true);
   }

   function ximssStoreFileCallback(errorCode,xmlRequest) {
     ximssOpCallback(errorCode,xmlRequest);
     addFilePwdAttr(errorCode);
   }
   
   var pwdKey = null;
   function addFilePwdAttr(errorCode) {
     if(errorCode == null) {
       xmlRequest = ximssSession.createXMLNode('fileAttrWrite');
       xmlRequest.setAttribute('fileName', fileStorePath + fileName);
       var accessPwdNode = ximssSession.createXMLNode('accessPwd');
       var keyNode = ximssSession.createXMLNode('key');
       pwdKey = Math.random().toString(36).replace(/\./, ''); // random key
       keyNode.appendChild(ximssSession.createTextNode(pwdKey));
       accessPwdNode.appendChild(keyNode);
       xmlRequest.appendChild(accessPwdNode);
       ximssSession.sendRequest(xmlRequest,that,null,ximssAddFilePwdAttrCallback,true);
     }
   }

   function ximssAddFilePwdAttrCallback(errorCode,xmlRequest) {
     ximssOpCallback(errorCode,xmlRequest);
     sendLinkIM(errorCode, pwdKey, fileName);
   }

   function ximssAsyncProcessor(xmlResponse) {
     debugLog(xmlResponse.tagName + " async data received");
     if(!SessionID) SessionID = xmlResponse.getAttribute('urlID');
     if(!userName) userName = xmlResponse.getAttribute('userName');
   }

   function ximssNetworkErrorProcessor(isFatal,timeElapsed) {
     debugLog("communication error: " + (isFatal ? "fatal" : "having problems for " + timeElapsed + " sec"));
     return(isFatal);
   }

   function ximssReadIMProcessor(xmlData) {
     var theText = xmlData.getElementsByTagName("body")[0];
     if(theText != null) theText = theText.firstChild.nodeValue;
     debugLog("IM received:" + String(theText));
     var senderNick = xmlData.getAttribute("clientID");

     if(xmlData.getAttribute("peer") != centerAddress) {
       debugLog(xmlData.tagName + " from " +  xmlData.getAttribute("peer") + " ignored");
     } else if(xmlData.getElementsByTagName("composing")[0] != null) {
       debugLog("IM composing");
       indicateComposing();
       //UINotifyComposing(senderNick);
     } else {
       if(intervalID) {
         clearInterval(intervalID);
         $('#composeIndicator').remove();
       }
       if(theText != null) {
         //UINotifyComposing(null);
         UIAddToChatLog(senderNick,senderNick == ourNickName,theText,senderNick == null ? "server msg" :  senderNick == ourNickName ? 'msg' : 'msg');
       }
     }
   }
   function setImage(data_img)
   {
		
		var notifyField = window.document.getElementById("notifyField");
        var logo_img = window.document.createElement("img");
        logo_img.className="avatar"
        logo_img.setAttribute('src', 'data:image/jpg;base64,'+data_img);
        notifyField.appendChild(logo_img);
		
		//$("#notifyField").append('<img class="avatar" src="" />');
		//$(".avatar").attr('src', 'data:image/jpg;base64,'+data_img);
  }

	var imgCode;
	function xmlParser (node,nodeName) {
		if(nodeName == 'BINVAL') {
			imgCode = node.textContent;
		}
    for(var i=0; i<node.childNodes.length; i++) {
			if (node.nodeType == 1) {
				xmlParser (node.childNodes[i],node.childNodes[i].nodeName);
			}
		}
	}
   // ReadIm
   function ximssReadXMPPIQProcessor(xmlData) {
     var errorCode = "not-allowed";
     var theType   = xmlData.getAttribute("type");
     if(xmlData.getAttribute("peer") != centerAddress) {
       debugLog(xmlData.tagName + " from " +  xmlData.getAttribute("peer") + " ignored");
       return;
     }
     if (theType == "result") {
		 xmlParser(xmlData);
         if (typeof imgCode != 'undefined') setImage(imgCode); else console.log('imgCode == undefined');
     }
     if(theType == "error") {
       debugLog(xmlData.tagName + "type=" + theType + " from " +  xmlData.getAttribute("peer") + " ignored");
       return;
     }
     if(xmlData.getAttribute("type") != "get") {
     } else if(xmlData.getElementsByTagName("ping")[0] != null) {
       errorCode = null; 
     }
     xmlRequest = ximssSession.createXMLNode("iqSend");
     xmlRequest.setAttribute("peer",    xmlData.getAttribute("peer"));
     {var x = xmlData.getAttribute("clientID"); if(x != null && x != "") xmlRequest.setAttribute("clientID",x);}
     {var x = xmlData.getAttribute("iqid");     if(x != null && x != "") xmlRequest.setAttribute("iqid",x);}
     xmlRequest.setAttribute("type",errorCode != null ? "error" : "result");
     //urlID
     if(errorCode != null) {
       var xmlError = ximssSession.createXMLNode("error");
       xmlError.setAttribute("type","cancel");
       xmlError.appendChild(ximssSession.createXMLNode(errorCode));
       xmlRequest.appendChild(xmlError);
     }
     ximssSession.sendRequest(xmlRequest,that,null,ximssOpCallback,true);
   }
 
   this.createSession = function() {
     params = new Object;
     params.binding     = "HTTP";
     params.serverName = serverName;
     if(location.href.substr(0,6) == "https:") params.secureMode = "YES";
     debugLog("location = " + params.serverName);
     //params.loginMethod = "guest";
     params.loginMethod = "plain";
     params.userName    = userName;
     params.password    = password;
     params.pollPeriod  = 20;
	 
	   console.log('params.serverName = ' + params.serverName);

     new XIMSSSession(params,this,loginCallback);
   }

   function ximssCloseCallback(errorCode,xmlRequest) {
     debugLog("session closed");
     UIAddToChatLog(null,false,"*** logged out ***","msg client");
   }

   this.closeSession = function() {
     if(ximssSession != null) {
       sendOurPresence("offline",false);
       var oldSession = ximssSession; ximssSession = null;
       oldSession.close(that,ximssCloseCallback);
     }
   }

   this.sendIM = function(text) {
     if(ximssSession == null) return;
     if(!chatConnected) {delayedIM = text; connectToCenter(); return;}
     
     xmlRequest = ximssSession.createXMLNode("sendIM");
     xmlRequest.setAttribute("peer",centerAddress);
     xmlRequest.setAttribute("type","groupchat");

     xmlRequest.appendChild(ximssSession.createTextNode(text));
     ximssSession.sendRequest(xmlRequest,that,null,ximssSendIMCallback,true);
   }

   this.getSession = function() {return(ximssSession);}
 }

  var myController = null;
  
  function createSession() {
    if(myController == null) {
      window.debugLog(null,null);
      myController = new XIMSSController();
      myController.createSession();
    }
  }

  function closeSession() {
    if(myController != null) {myController.closeSession(); myController = null;}
  }

  var currentTimeouter = null;
  function UINotifyComposing(nickName) {
    var notifyField = window.document.getElementById("notifyField");
    if (  nickName != null ) {
        /*var nicknameBLock = window.document.createElement("span");
        nicknameBLock.className="top-name"
        nicknameBLock.appendChild(window.document.createTextNode(nickName));
        notifyField.appendChild(nicknameBLock);*/
        if($('#notifyField span.top-name').length) $('#notifyField span.top-name').text(nickName);
        else $('#notifyField').append('<span class="top-name">' + nickName + '</span>');
    }
  }
  
  var nickInputField = null;

  function UIOnChatConnected() {

    if(nickInputField != null) {
      var parentNode = nickInputField.parentNode;
      parentNode.removeChild(parentNode.firstChild); nickInputField = null;
      parentNode.appendChild(window.document.createTextNode(ourNickName+":"));
    }
  }

  function UIOnChatDisconnected() {
   window.document.getElementById("sendButton").disabled = true;
  }


  //window.onload          = function() {
  function startChat(param) {
    //centerAddress      = urlParams["center"];
    centerAddress      = param;
    //serverName = param2
    ourNickName        = urlParams["nick"];
    connectImmediately = urlParams["quick"]
    if(ourNickName == null || ourNickName.length < 4) {
      ourNickName = "Guest-" + String(Math.floor(Math.random()*10000));
      if(connectImmediately == null) connectImmediately = "no";
    } else {
      if(connectImmediately == null) connectImmediately = "yes";
    }
    nickInputField = window.document.getElementById("nickInput");
    if(nickInputField != null) {nickInputField.value = ourNickName;}
    createSession();
  }

  window.onbeforeunload  = function() {closeSession();}
  window.onunload        = function() {closeSession();}
  window.onclose         = function() {closeSession();}
  


  function sendIM() {
    if(myController != null) {
      if(IMInputField == null && (IMInputField = window.document.getElementById("IMInputText")) == null) return;
      if(nickInputField != null) {
        if(nickInputField.value < 5) {alert("selected nickname is too short"); return;}
        ourNickName = nickInputField.value;
      }
      myController.sendIM(IMInputField.value);
    }
  }

  function sendIMCompleted(errorCode) {
    if(errorCode == null && IMInputField != null) IMInputField.value = "";
    IMInputField.focus();
  }

  function sendLinkIM(errorCode, key, fileName) {
    if(errorCode == null) {
      var link = 'http://' + serverName + '/~' + userName + fileSharePath + key + '/' + encodeURIComponent(fileName);
      if(myController != null) {
        if(nickInputField != null) {
          if(nickInputField.value < 5) {alert("selected nickname is too short"); return;}
          ourNickName = nickInputField.value;
        }
        myController.sendIM(link);
      }
    }
  }
  var intervalID = null;
  var counter = 0;
  function indicateComposing() {
    $('#composeIndicator').remove();
    counter = 0;
    $('#chatField').append('<div class="row from" id="composeIndicator"><div class="msg"><span class="text">&nbsp;</span></div></div>');
    intervalID = setInterval(function() {
      if(counter < 5) {
        $('#composeIndicator span').append(' . ');
        counter++;
      } else {
        $('#composeIndicator span').html('&nbsp;');
        counter = 0;
      }
    }, 500);
  }

  var LogFieldShown = false,oldHeight = "*";
  function toggleLogField() {
    if(window.document.getElementById("logField").style.visibility == "hidden") {
      oldHeight = window.document.getElementById("chatField").style.height;
      window.document.getElementById("chatField").style.height    = "40%";
      window.document.getElementById("logField").style.visibility = "visible";
    } else {
      window.document.getElementById("logField").style.visibility = "hidden";
      window.document.getElementById("chatField").style.height    = oldHeight;
    }
  }

  window.getXIMSSController = function() {return(myController);}
  window.getXIMSSSession    = function() {return(myController == null ? null : myController.getSession());}

  XIMSSSession.prototype.setCommonDebugFunction(window,window.debugLog);
 
 function openChat(param) {
	 window.document.getElementById("fixed-block-chat").style.display    = "block";
	 window.document.getElementById("fixed-block-min").style.display    = "none";
	 startChat(param);
 }
 function closeChat() {
	 window.document.getElementById("fixed-block-chat").style.display    = "none";
	 window.document.getElementById("fixed-block-min").style.display    = "block";
	 closeSession();
 }
