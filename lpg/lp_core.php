<?php
$slack_token = "xoxp-8908172709-8908081702-10271090480-586e1e";
$slack_default = "C0A7ZCU05";
include ('slack.php');

// API_KEY set by pre_file
$base_url = "https://api.pipedrive.com/v1/";
$map = array(
	'phone' => 'phone',
	'name' => 'name',
	'url' => 'url'
);

define ('R_GET', 'GET');
define ('R_POST', 'POST');

// Serialization
function array_serialize($in, $html = false) {
	$out = "";
	
	foreach ($in as $k => $v) {
		$out .= "$k: $v";
		$out .= ($html) ? "<br />" : " ";
	}
	return $out;
}

// Soul of a new machine
function r($url, $params = null, $method = R_GET) {
	global $base_url;
	if ($params === null) {$params = array();}
	$result = file_get_contents($url, false, stream_context_create(array(
    	'http' => array(
    		'host' => $base_url,
    	    'method'  => $method,
    	    'header'  => 'Content-type: application/json',
    	    'content' => json_encode ($params)
    	)
	)));

	return $result;
}	

// Wrapper
function w($zone, $path, $body = null, $method = R_GET) {
	global $api_key;
	global $base_url;

	// $path = ($path === null) ? null : "$path&";
	$url = $base_url."$zone?$path&api_token=$api_key";

	return json_decode(r($url, $body, $method));
}

// Get actual phone
function refine_phone($dirty_phone) {
	// 8 (123) 456-78-90 -> 1234567890
	preg_match_all('!\d+!', $dirty_phone, $matches);
	return substr(implode($matches[0]), -10);
}


// Get POST data
// var_dump ($_POST);

$phone = (!isset($phone)) ? $_POST[$map['phone']] : $phone;
$name = (!isset($name)) ? $_POST[$map['name']] : $name;
$web = (!isset($web)) ? $_POST[$map['url']] : $web;
$raw = (!isset($raw)) ? $_POST : $raw;

$rphone = refine_phone($phone);
$usr = w('searchResults', "term=$rphone&item_type=person");
$usr = $usr->data;

// Refine phone
// $phone = refine_phone($phone);

if ($usr === null) {
	// Create new user...

	// Prepare data-string
	$crt = array(
		'name' => $name,
		'phone' => array(
			'value' => $phone
			)
	);	

	// Create user
	$usr = w('persons', '', $crt, R_POST);
	
	// Get User_id
	$uid = $usr->data->id;

} else {
	$uid = $usr[0]->id;
}

// Create deal
$crt = array(
	'person_id' => $uid,
	'stage_id' => $stage_id,
	'title' => "$name ($phone), МАДАГАСКАР заявка с $web"	
	);
$deal = w('deals', '', $crt, R_POST);
$did = $deal->data->id;

// Create activity

//Get RoiStat value
$roistatVisitId = array_key_exists('roistat_visit', $_COOKIE) ? $_COOKIE['roistat_visit'] : "неизвестно";

// If roiStatField not defined, just set it to null
if (!$roiStatField) {
	$roiStatField = 'null';
}

$crt = array(
	'deal_id' => $did,
	'subject' => "МАДАГАСКАР - Оставлена заявка с сайта $web",
	'type' => $activity_keystring,
	'note' => array_serialize ($raw),
	'done' => 1,
	$roiStatField => $roistatVisitId
	);
$act = w('activities', '', $crt, R_POST);

// Post to Slack
if (!isset($slack_channel)) {
	$slack_channel = $slack_default;
}

$text = "Новый лид: $name ($phone) [$web], ".array_serialize($raw);
$sl = new Slack($slack_token);
$sl->msg($slack_channel, $text);

// RoiStat deal-proxy
/*
$roistatData = array(
    'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
    'key'     => 'NTg0Mjo2NDc1OmJkZjQwMTlmZjlmYTRiYjAxOGI5OGMwYWVmMTQ4M2Y3 ', // Замените SECRET_KEY на секретный ключ из пункта меню Настройки -> Интеграция со сделками в нижней части экрана и строчке Ключ для интеграций
    'title'   => "$name ($phone), заявка с $web",
    'comment' => array_serialize ($raw),
    'name'    => $name,
    'email'   => $email,
    'phone'   => $phone,
    'fields'  => array(
    ),
);
      
file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
*/

// Write log
if (isset($logfile)) {
	file_put_contents($logfile, array_serialize($raw)."\r\n", FILE_APPEND);
}

// Go to thank you?
if (isset($thankyou)) {
	header ("Location: $thankyou");
}


?>