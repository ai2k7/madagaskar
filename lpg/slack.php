<?php
// define('token', '');

class Slack {
	function __construct($token) {
		$this->token = $token;
		$this->url = "https://slack.com/api/";
	}

	function msg($channel, $message) {
		return $this->make_request("chat.postMessage", array(
			"channel" => $channel,
			"text" => $message
		));
	}

	/* Private API */
	function make_request($zone, $options) {
		$options['token'] = $this->token;

		$zone = 'chat.postMessage';
		$url = $this->url."$zone?".http_build_query($options);
		// return http_get($url);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
}
?>